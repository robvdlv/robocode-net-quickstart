@echo off
echo This will register bot dlls with the GAC.
echo.

:: well known paths
set dll_folder=%~dp0reactive-bot\ReactiveBot\bin\Release

: find gacutil
echo Locating gacutil on C: ...
for /f "usebackq delims=|" %%f in (`dir c:\gacutil.exe /s/b /o:N`) do (
	set gac_util=%%~sf
)
if /%gac_util%/==// (
	echo Did not find gacutil on your C: drive! This is required in order to continue.
	goto END
)
echo Found gacutil in [%gac_util%]
echo.

for /f "usebackq delims=|" %%f in (`dir %dll_folder%\*.dll /s/b /o:N`) do (
	echo Registering with GAC: [%%~fnf]
	%gac_util% /i %%f
)
echo.
echo Done!
echo.

:END
pause
