﻿using System;
using System.Reactive.Linq;
using ReactiveBot.Rx;
using Robocode;

namespace ReactiveBot
{
    public class ReactiveBot : Robot
    {
        private BotSense _sense;

        // The main method of your robot containing robot logics
        public override void Run()
        {
            InitBot();

            // Infinite loop making sure this robot runs till the end of the battle round
            while (true)
            {
                // -- Commands that are repeated forever --

                // Move our robot 5000 pixels ahead
                Ahead(5000);

                // Turn the robot 90 degrees
                TurnRight(90);

                // Our robot will move along the borders of the battle field
                // by repeating the above two statements.
            }
        }

        private void InitBot()
        {
            Console.WriteLine("init");

            _sense = new BotSense();
            _sense.Where(e => e is ScannedRobotEvent).Cast<ScannedRobotEvent>().Subscribe(new Gunner(this));

            Console.WriteLine("init");
        }

        // Robot event handler, when the robot sees another robot
        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            _sense.PublishEvent(e);
        }

        class Gunner : ObserverSupport<ScannedRobotEvent>
        {
            private readonly Robot _robot;

            public Gunner(Robot robot)
            {
                _robot = robot;
            }

            public override void OnNext(ScannedRobotEvent value)
            {
                _robot.Fire(1);
            }
        }
    }
}
