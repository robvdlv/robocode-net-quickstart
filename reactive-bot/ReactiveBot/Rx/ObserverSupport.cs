using System;

namespace ReactiveBot.Rx
{
    public abstract class ObserverSupport<T> : IObserver<T>
    {
        public abstract void OnNext(T value);

        public virtual void OnError(Exception error)
        {
        }

        public virtual void OnCompleted()
        {
        }
    }
}
