# Robocode .NET Quickstart

Using .NET to implement your own [Robocode](http://robocode.sourceforge.net/) robot?

This quickstart is going to get you up and running in no time.


## Setting up Robocode

* Make sure Java SDK is installed. We will need Java to run the Robocode game host, which we want to run to test our robots.
    
  [Download the Java SDK.](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
  
  
* Make sure JAVA_HOME system environment variable is properly configured.

  - Press keys : Windows + Pause/Break
  - Click link : Advanced System Settings
  - Click tab : Advanced
  - Click button : Environment variables
  - Add new SYSTEM variable : ```JAVA_HOME```
      with value: ```path/to/java/sdk```; e.g. ```C:\Program Files\Java\jdk1.7.0_51```

* Clone this Git repository.

  ```git clone https://bitbucket.org/robvdlv/robocode-net-quickstart.git```

* You should now be able to start Robocode by running ```run-robocode.cmd```.

## Building our sample bot

* Open the sample ReactiveBot Visual Studio solution ```reactivebot\ReactiveBot.sln```
* Build the solution.

## Setting up Robocode to run our bot

* Start Robocode
* Open ```Options``` / ```Preferences``` / ```Development options```
* Add the path to the DLL of the bot you just build. E.g. ```c:\robocode-net-quickstart\reactive-bot\bin\Release```. This is a one-time-step.
* Press ```Finish```.

## Run your bot in a battle

* Open ```Battle``` / ```New``` / Select your bot and at least another bot.
* Choose ```Start battle```.

# More information
* [Robocode Website](http://robocode.sourceforge.net/)
* [Robocode .NET API docs](http://robocode.sourceforge.net/docs/robocode.dotnet/Index.html)
