@echo off

:: where we are now
set this_directory=%~dp0

:: run robocode
cd robocode-1.9.0
call robocode.bat

:: and we're back!
cd %this_directory%
